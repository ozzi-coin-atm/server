Rails.application.routes.draw do
  resources :transactions
  resources :users
  get '/users/prox/:prox', to:'users#show_by_prox'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
