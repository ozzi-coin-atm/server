json.extract! user, :id, :name, :prox_num, :balance, :created_at, :updated_at
json.url user_url(user, format: :json)
