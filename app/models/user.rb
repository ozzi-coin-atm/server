class User < ApplicationRecord
    has_many :transactions

    def balance
        transactions.sum(:value)
    end
end
